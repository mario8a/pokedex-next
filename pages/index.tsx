import { Grid} from '@nextui-org/react';
import { NextPage, GetStaticProps } from 'next'
import { pokeApi } from '../api';
import { PokemonCard } from '../components/pokemon';
import { PokemonListResponse, SmallPokemon } from '../interfaces';
import { Layout } from './../components/layouts';

interface Props {
  pokemons: SmallPokemon[]
}

const HomePage: NextPage<Props> = ({pokemons}) => {
  return (
    <>
    <Layout title='Listado de pokemons'>
      <Grid.Container gap={2} justify="flex-start">
        {
          pokemons.map((pokemon) => (
            <PokemonCard key={pokemon.id} pokemon={pokemon}/>
          ))
        }
      </Grid.Container>
    </Layout>
    </>
  )
}

// Solo se ejecuta en build time / del lado del servidor
// Solo se puede usar en pages
export const getStaticProps: GetStaticProps = async (ctx) => {

  const {data} = await pokeApi.get<PokemonListResponse>('/pokemon?limit=150');
  const pokemons: SmallPokemon[] = data.results.map((pokemon, i) => {
    return {
      ...pokemon,
      id: i + 1,
      img: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${i + 1}.png`
    }
  });

  return {
    props: {
      pokemons
    }
  }
}

export default HomePage
